import { Injectable } from '@angular/core';
import { Aerolinea } from './aerolinea.model';
import { HttpClient } from '@angular/common/http';
import { Form } from '@angular/forms';

@Injectable()
export class AerolineaService {

  formData: Aerolinea = {
    id: null,
    nombre: null,
    direccion: null
  };

  readonly rootURL = 'http://localhost:5000/api/';
  listadoAerolineas: Aerolinea[];

  constructor(private http: HttpClient) { }

  insertAerolinea(){
    return this.http.post(this.rootURL+'Aerolineas', this.formData);
  }

  deleteAerolinea(id){
    console.log(id)
    return this.http.delete('http://localhost:5000/api/aerolineas/'+id);
  }

  updateAerolinea(){
    return this.http.put(this.rootURL+'Aerolineas/'+this.formData.id, this.formData );
  }

  listarAerolineas(){
    this.http.get(this.rootURL + 'Aerolineas')
    .toPromise()
    .then(res => this.listadoAerolineas = res as Aerolinea[]);
  }

}
