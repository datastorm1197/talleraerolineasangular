import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {FormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import { AerolineasComponent } from './aerolineas/aerolineas.component';
import { AerolineaComponent } from './aerolineas/aerolinea/aerolinea.component';
import { AerolineaListComponent } from './aerolineas/aerolinea-list/aerolinea-list.component';
import { AerolineaService } from './shared/aerolinea.service';
import { HttpClientModule } from '@angular/common/http';



@NgModule({
  declarations: [
    AppComponent,
    AerolineasComponent,
    AerolineaComponent,
    AerolineaListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [AerolineaService],
  bootstrap: [AppComponent]
})
export class AppModule { }
