import { Component, OnInit } from '@angular/core';
import { Aerolinea } from '../../shared/aerolinea.model';
import { AerolineaService } from '../../shared/aerolinea.service';

@Component({
  selector: 'app-aerolinea-list',
  templateUrl: './aerolinea-list.component.html',
  styles: []
})
export class AerolineaListComponent implements OnInit {

  constructor(private service: AerolineaService) { }

  ngOnInit() {
    this.service.listarAerolineas();
  }

  populateForm(aero: Aerolinea) {
    this.service.formData = Object.assign({}, aero);
  }

  eliminarAerolinea(id){
    if(confirm('Desea borrar esta aerolínea')){
      this.service.deleteAerolinea(id).subscribe(res =>{
        this.service.listarAerolineas();
      }, err =>{
        
      });
    }

  }


}
