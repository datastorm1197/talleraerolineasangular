import { Component, OnInit } from '@angular/core';
import {AerolineaService} from '../../shared/aerolinea.service';
import { Form, NgForm } from '@angular/forms';

@Component({
  selector: 'app-aerolinea',
  templateUrl: './aerolinea.component.html',
  styles: []
})
export class AerolineaComponent implements OnInit {

  constructor(private service: AerolineaService) { }

  ngOnInit() {
    this.resetForm();
  }

  resetForm(form?: NgForm) {
    if (form != null)
      form.form.reset();
    this.service.formData = {
      id: 0,
      nombre: '',
      direccion: ''
    }
  }

  insertRecord(form: NgForm) {
    this.service.insertAerolinea().subscribe(
      res => {
        this.resetForm(form);
        console.log('Se insertó un registro');
        window.alert('Se inserto una aerolínea');
        this.service.listarAerolineas();
      },
      err => {
        console.log(err);
      }
    )
  }

  updateRecord(form: NgForm){
    this.service.updateAerolinea()
    .subscribe(res => {
      window.alert('Se ha actualizado la aerolínea');
      this.service.listarAerolineas();
    }, err => {

    });
  }

  onSubmit(form: NgForm){
    if(this.service.formData.id == 0){
      this.insertRecord(form);
    }else{
      this.updateRecord(form);
    }
    this.resetForm();
  }

}
